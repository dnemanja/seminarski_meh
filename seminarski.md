## Motion Capture

![alt text](0.PNG)
Dijagram automobila koji se spusta niz brdo pod nekim uglom \(\theta\) i koji krece iz stanja mirovanja sa neke visine h.

![alt text](s.png)

Auto se nalazi na uzvisenju koje se nalazi pod uglom u odnosu na horizontalnu osu. Sile koje deluju na automobil su: sila zemljine teze \(m\vec{g}\) i sila reakcije podloge \(\vec{N}\).
![alt text](a1.PNG)

Silu zemljine teze \(m\vec{g}\) mozemo da projektujemo na pravac u kome deluje sila reakcije podloge \(mg\cos\theta\) i pravac normalana na pravac dejstva sile reakcije podloge \(mg\sin\theta\) :
![alt text](a2.png)

Posto se auto ne krece u pravcu dejstva sile reakcije podloge, to znaci da je sila reakcije \(\vec{N}\) po intenzitetu jednaka sili \(mg\cos\theta\). Sada cemo silu \(\vec{N}\) oznaciti silom \(m\vec{g}\cos\theta\), jer je to njen intenzitet; i projektovati na x i y osu :
![alt text](a3.PNG)

Sada koristimo drugi njutnov zakon: proizvod mase i ubrzanja tela jednak je zbiru svih sila koje deluju na to telo:
![alt text](a4.PNG)

Vrsimo projekciju drugog njutnovog zakona na x i y pravac:
![alt text](a5.PNG)
Imamo –mg jer je y osa usmerena na gore, a sila zemljine teze deluje na dole.

Pokratimo masu i dobijamo:
![alt text](a6.PNG)

\(\ddot{x}\) je ubrazanje po x osi, a \(\ddot{y}\) je ubrzanje po y osi.

![alt text](analiticko.jpg)
