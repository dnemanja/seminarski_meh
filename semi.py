import numpy as np
from matplotlib import pyplot as plt

#ucitavanje podataka
t1, a1x, a1y = np.loadtxt('motion1.d', usecols=[0,1,2], unpack=True);
t2, a2x, a2y = np.loadtxt('motion2.d', usecols=[0,1,2], unpack=True);

n1 = len(t1)

v1x = np.zeros((n1),float)
v1y = np.zeros((n1),float)

r1x = np.zeros((n1),float)
r1y = np.zeros((n1),float)


n2 = len(t2)

v2x = np.zeros((n2),float)
v2y = np.zeros((n2),float)

r2x = np.zeros((n2),float)
r2y = np.zeros((n2),float)


#inicijalizacija magnitude nizova
maga1 = np.zeros((n1),float)
maga2 = np.zeros((n2),float)

#visina h=30
r1y[0] = 30
r2y[0] = 30

#magnitude of an vector
def mag(x, y):
    return (x ** 2 +  y ** 2) ** 0.5


#racunanje r1 i v1
for i in range(0, n1-1):
    dt1 = t1[i+1] - t1[i]
    
    v1x[i+1] = v1x[i] + a1x[i]*dt1
    v1y[i+1] = v1y[i] + a1y[i]*dt1
    
    r1x[i+1] = r1x[i] + v1x[i+1]*dt1
    r1y[i+1] = r1y[i] + v1y[i+1]*dt1
    

    
    maga1[i] = mag(a1x[i], a1y[i])
    
    

#racunanje r2 i v2
for i in range(0, n2-1):
    dt2 = t2[i+1] - t2[i]
    
    v2x[i+1] = v2x[i] + a2x[i]*dt2
    v2y[i+1] = v2y[i] + a2y[i]*dt2
    
    r2x[i+1] = r2x[i] + v2x[i+1]*dt2
    r2y[i+1] = r2y[i] + v2y[i+1]*dt2
    

    maga2[i] = mag(a2x[i], a2y[i])



#plotovanje
plt.figure(figsize=(20,8))
plt.plot(r1x, r1y)
plt.plot(r2x, r2y)

#argmax vraca indeks najveceg elementa u nizu
id1 = np.argmax(maga1)
plt.annotate("  max a1", (r1x[id1], r1y[id1]))
plt.scatter(r1x[id1], r1y[id1])

id2 = np.argmax(maga2)
plt.annotate("  max a2", (r2x[id2], r2y[id2]))
plt.scatter(r2x[id2], r2y[id2])

plt.xlabel("x [m]")
plt.ylabel("y [m]")
plt.axis('equal')
plt.show()





